#!/usr/bin/python

# This shows a simple example of an MQTT subscriber.

import random
from math import sin, cos, sqrt, fabs
import time

import mosquitto


global state
state = {}


def on_connect(mqttc, obj, rc):
    print("rc: " + str(rc))


def process_shot(player, angle, force):
    global state
    v = 500.0 * force
    g = 100.0
    print("v = %s, g = %s" % (v,g))

    start_pos = state['position' + str(player)]
    print("sp = %s" % (start_pos, ))
    v_cur = (v * cos(angle) * (1 if player == 1 else -1) + state['wind'], v * sin(angle))


    print("v_cur = %s" % (v_cur, ))

    t = 2.0 * v_cur[1] / g
    x_imp = start_pos + v_cur[0] * t

    print("x_imp = %s" % (x_imp, ))

    return x_imp


def analyze_impact(player, loc):
    global state

    other_player = 2 if player == 1 else 1
    start_pos = state['position' + str(player)]
    target_pos = state['position' + str(other_player)]

    if target_pos > start_pos:
        if loc - 25 > target_pos:
            return 'far'
        elif loc + 25 < target_pos:
            return 'near'
        else:
            return 'hit'
    else:
        if loc + 25 > target_pos:
            return 'near'
        elif loc - 25 < target_pos:
            return 'far'
        else:
            return 'hit'


def on_message(mqttc, obj, msg):
    global state

    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))

    player = 1
    if msg.topic == "cannons/cannon1/fire":
        player = 1
    elif msg.topic == "cannons/cannon2/fire":
        player = 2

    if player not in [1, 2]:
        return

    if player != state['turn']:
        return

    other_player = 2 if player == 1 else 1
    angle, force = msg.payload.split(',')
    angle, force = float(angle), float(force)
    force = 0.1 + force / 10.0
    angle = angle / 63.0 * (3.14159265 / 4.0)
    print("F: %s, A: %s" % (force, angle))
    shot_loc = process_shot(player, angle, force)
    shot_impact = analyze_impact(player, shot_loc)
    state['wind'] = random.uniform(-20, 20)

    if shot_impact == 'hit':
        hp = 'health' + str(other_player)
        state[hp] -= 1
        if state[hp] == 0:
            shot_impact = 'win'
            mqttc.publish('cannons/cannon%d/lose' % other_player, 'yousuck', 0, True)

    mqttc.publish('cannons/cannon%d/shoteffect' % player, shot_impact, 0, True)
    mqttc.publish('cannons/cannon1/wind', state['wind'], 0, True)
    mqttc.publish('cannons/cannon2/wind', state['wind'], 0, True)
    time.sleep(3)

    if shot_impact != 'win':
        mqttc.publish("cannons/cannon%d/statechange" % other_player, 'play', 0, True)
        mqttc.publish("cannons/cannon%d/statechange" % player, 'wait', 0, True)

    state['turn'] = other_player


def on_publish(mqttc, obj, mid):
    print("mid: " + str(mid))


def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))


def on_log(mqttc, obj, level, string):
    print(string)

# If you want to use a specific client id, use
# mqttc = mqtt.Client("client-id")
# but note that the client id must be unique on the broker. Leaving the client
# id parameter empty will generate a random id for you.
mqttc = mosquitto.Mosquitto()
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe
# Uncomment to enable debug messages
# mqttc.on_log = on_log
# mqttc.connect("127.0.0.1", 1883, 60)

# setting testament for that client
# mqttc.will_set("temp/floor1/room1/pref1", "broken", 0, True)

state['position1'] = random.uniform(0, 500)
state['position2'] = 550 + random.uniform(0, 500)
state['wind'] = random.uniform(-20, 20)
state['turn'] = 1
state['health1'] = 2
state['health2'] = 2

print("pos = %s, %s" % (state['position1'], state['position2']))

mqttc.connect("127.0.0.1", 1883, 60)
mqttc.publish("cannons/cannon1/wind", state['wind'], 0, True)
mqttc.publish("cannons/cannon2/wind", state['wind'], 0, True)
mqttc.publish("cannons/cannon1/statechange", 'play', 0, True)
mqttc.publish("cannons/cannon2/statechange", 'wait', 0, True)

mqttc.subscribe("cannons/cannon1/fire", 0)
mqttc.subscribe("cannons/cannon2/fire", 0)

# publishing message on topic with QoS 0 and the message is not Retained
# mqttc.publish("temp/floor1/room1/pref1", "20", 0, False)

mqttc.loop_forever()
