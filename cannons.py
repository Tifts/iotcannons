#!/usr/bin/python

import sys
import threading
import signal
import traceback
import time

from serial import Serial

import mosquitto
from copernicus_api.copernicus import Copernicus


serial = None if sys.argv[2] is None else Serial(sys.argv[2], 38400, timeout=0.05)
global device
device = Copernicus(serial)

# play/wait
global game_state
game_state = "wait"
# angle - knob
global angle
angle = 0
# power - time button1 is pressed
global power
power = 0
# wind - get from controller, set dashboard
global wind
wind = 15
global cannon_name
cannon_name = "cannon1"


def on_connect(mqttc, obj, rc):
    print("rc: " + str(rc))


def on_message(mqttc, obj, msg):
    global game_state
    global wind
    global device
    global cannon_name
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))

    if game_state == "play":
        if str(msg.topic) == "cannons/" + cannon_name + "/statechange":
            game_state = msg.payload
            device.command('led', False)
            device.command('rgb', 0, 0, 0)
        if str(msg.topic) == "cannons/" + cannon_name + "/shoteffect":
            if msg.payload == "far":
                device.command('rgb', 'green')
            if msg.payload == "near":
                device.command('led', True)
            if msg.payload == "hit":
                device.command('rgb', 'blue')
                device.command('led', True)
            if msg.payload == "win":
                device.command('rgb', 'green')
                device.command('led', True)
        time.sleep(3)
    elif game_state == "wait":
        if str(msg.topic) == "cannons/" + cannon_name + "/statechange":
            print("Received statechange: " + msg.payload + ".")
            game_state = msg.payload
            device.command('led', False)
            device.command('rgb', 0, 0, 0)
        if str(msg.topic) == "cannons/" + cannon_name + "/wind":
            wind = float(msg.payload)
            wind = int(1.0 + ((20.0 + wind) / 40.0) * 30.0)
            device.command('servo', wind)
        if str(msg.topic) == "cannons/" + cannon_name + "/lose":
            device.command('rgb', 'red')
            device.command('led', True)


def on_publish(mqttc, obj, mid):
    print("mid: " + str(mid))


def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))


def on_log(mqttc, obj, level, string):
    print(string)

# If you want to use a specific client id, use
# mqttc = mqtt.Client("client-id")
# but note that the client id must be unique on the broker. Leaving the client
# id parameter empty will generate a random id for you.

cannon_name = sys.argv[1]
mqttc = mosquitto.Mosquitto()
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe
# Uncomment to enable debug messages
#mqttc.on_log = on_log

# setting testament for that client
#mqttc.will_set("temp/floor1/room1/pref1", "broken", 0, True)

mqttc.connect("127.0.0.1", 1883, 60)
mqttc.subscribe("cannons/" + cannon_name + "/+", 0)

global i, r, g, b
i, r, g, b = 0, 0, 0, 0


def power_button_press(state):
    global game_state
    global i, r, g, b
    print("Power button pressed at state: " + str(state) + ", game state: " + game_state)
    if game_state == "play":
        global power
        if state:
            if i == 0 and b < 3:
                b += 1
            elif i < 2 and g < 3:
                b = 0
                i = 1
                g += 1
            elif r < 3:
                g = 0
                i = 2
                r += 1
            else:
                i, r, g, b, power = 0, 0, 0, 0, 0
            device.command('rgb', r, g, b)
            power += 1


def fire_button_press(state):
    global game_state
    print("Fire pressed at state: " + str(state) + ", game state: " + game_state)
    if game_state == "play":
        if state:
            mqttc.publish("cannons/" + cannon_name + "/fire", str(angle) + "," + str(power), 0, True)


def set_angle(state):
    global angle
    angle = int(state)


device.command('subscribe', '*')
device.set_handler('button1', power_button_press)
device.set_handler('button2', fire_button_press)
device.set_handler('knob', set_angle)


def signal_handler(signal, frame):
    for th in threading.enumerate():
        print(th)
        traceback.print_stack(sys._current_frames()[th.ident])
        print()
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)

while True:
    device.listen()
    mqttc.loop(timeout=0.05)



